/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.doctor.mapper;

import com.curso.doctor.model.entity.Doctor;
import com.curso.doctor.model.entity.DoctorDto;
import org.mapstruct.Mapper;

@Mapper
public interface DoctorMapper {
    
    DoctorDto doctorToDoctorDto(Doctor doctor);
    
    Doctor doctorDtoToDoctor(DoctorDto doctorDto);
}
