/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.doctor.controller;

import com.curso.doctor.model.entity.DoctorDto;
import com.curso.doctor.service.IDoctorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/doctor")
public class DoctorController {
    
    @Autowired
    IDoctorService doctorService;
    
    @PostMapping("/guardarDoctor")
    public DoctorDto guardarDoctor(@RequestBody final DoctorDto doctorDto){
        
        return doctorService.guardar(doctorDto);
    }
    
    @GetMapping("/listarDoctores")
    public List<DoctorDto> listDoctores() {

        return doctorService.consultar();
    }
}
