/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.doctor.service;

import com.curso.doctor.model.entity.Doctor;
import com.curso.doctor.model.entity.DoctorDto;
import java.util.List;

/**
 *
 * @author ayanes
 */
public interface IDoctorService {
    
    List<DoctorDto> consultar();
    DoctorDto guardar(DoctorDto doctorDto);
}
