/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.doctor.service.impl;

import com.curso.doctor.mapper.DoctorMapper;
import com.curso.doctor.model.entity.Doctor;
import com.curso.doctor.model.entity.DoctorDto;
import com.curso.doctor.repository.DoctorRepository;
import com.curso.doctor.service.IDoctorService;
import java.util.ArrayList;
import java.util.List;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultDoctorService implements IDoctorService{

    @Autowired
    DoctorRepository doctorRespository;
    
    DoctorMapper doctorMapper = Mappers.getMapper(DoctorMapper.class);
    
    @Override
    public List<DoctorDto> consultar() {
        List<Doctor> doctors = doctorRespository.findAll();
        List<DoctorDto> doctorsDto = new ArrayList();
        
        for(Doctor d : doctors){
            DoctorDto doctorDto = doctorMapper.doctorToDoctorDto(d);
            doctorsDto.add(doctorDto);
        }
        
        return doctorsDto;
    }

    @Override
    public DoctorDto guardar(DoctorDto doctorDto) {
        Doctor doc = doctorMapper.doctorDtoToDoctor(doctorDto);
        doc = doctorRespository.save(doc);
        DoctorDto ddto = doctorMapper.doctorToDoctorDto(doc);
        return ddto;
    }
    
}
